# xenial-java9

Based on cenote/xenial-base

This Docker image is used to build java projects (ant, maven)

Installed software:

* openjdk-9-jdk-headless
* ant
* maven
